// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../interface/IFlashCallback.sol";
import "../interface/IFrankencoin.sol";

contract FlashLender is IFlashCallback {
    IFrankencoin immutable ZCHF;

    bool repayLoan;

    constructor(address zchf) {
        ZCHF = IFrankencoin(zchf);
    }

    function testFlash(uint amount, bool repay) external {
        repayLoan = repay;
        ZCHF.flash(address(this), amount, "");
    }

    function flashCallback(uint amount, bytes calldata data) external {
        require(ZCHF.balanceOf(address(this)) >= amount, "not received");
        if (repayLoan) ZCHF.transfer(address(ZCHF), amount);
    }
}
