// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IFlashCallback {
    function flashCallback(uint amount, bytes calldata data) external;
}
